# Your First AEK Project

## Create your First Project

Create a suitable directory to house your AEK projects and navigate to that directory:

```bash
mkdir test-applet
cd test-applet
```

We can now create a new AEK project based on a selected boilerplate.

You can create your own boilerplates but for now we will use
the basic iLancaster boilerplate.

```bash
aek create -b ilancaster-boilerplate-aek-react
```

**Note**: For more information about AEK commands
Please see [AEK CLI](https://npm.campusm.net/-/docs/@ombiel/aek-cli/0.2.7/)

You will need to answer the prompts provided with details like the package name and the hostname for the app you want to proxy, etc. You may also need to login with your iLancaster development credentials

Once you have completed all the prompts, the most recent version of the boilerplate should begin to download and all the required dependencies will be installed locally to that project.

When this has completed, you can navigate to the project and start it up.

```bash
cd my-new-project
aek start
```

This will startup a local server on port 5000.
Open up your favorite browser and enter the address:

```
http://localhost:5000
```

## Extra

- This is a fairly boring AEK project
- Try and change "main.jsx" to make it better.
