# Deploying an AEK Project to Production

As an iLancaster developer you will not be able to deploy your AEK projects to the iLancaster Production environment.

## From Beta to Local

Once you are satisfied with your AEK project in the iLancaster Beta environment.

You will then need to contact an iLancaster administrator, requesting
them to deploy your AEK project to the iLancaster Production environment.

Please provide the iLancaster administrator with your AEK project name.

## Review

The iLancaster team reviews all AEK projects submitted in an effort to determine
whether they are reliable, perform as expected, and are free of offensive material.
As you plan and develop your AEK project, please make sure to use
our guidelines and resources.

## Approval

Shortly after your AEK project has been approved it will be released into
the Production environment

## Rejection

If your AEK project has been rejected, the iLancaster team will provide information,
including any specific Guidelines that your AEK project did not follow.
