# AEK

iLancaster is built on the [CampusM](http://www.campusm.com/) framework.

CampusM, as part of their solution delivery, provide an Application Extension Kit
(AEK) as a modular framework. This framework is built from common 3rd party libraries
such as (React, Babel, Webpack, Gulp, etc).

Benefits:

- Delivers cross-platform application screens via a webview.
- A Framework that provides a responsive front-end to integrate with the CampusM platform.
- A web based environment for developing, testing and deploying application screens.

If you are not yet familiar with these tools, checkout our reading [list](https://npm.campusm.net/-/docs/@ombiel/aek-guide/1.0.1/pages/90-further-reading) for more information.

## Goals

- Provide a more sophisticated development environment.
- Promote client-side processing over server-side.
- New features through closer integration with the CampusM native clients.
- A modular approach to provide the greatest flexibility for customer's differing needs.
- Latest industry standard tools and techniques where possible.
- Make use of the thriving open source javascript community.
- Promote sharing and collaboration between CampusM customers.

## AEK Packages

AEK is made up of three basic modules:

aek-lib - AEK core client side JS libraries, including React components and CampusM integration tools

aek-css - AEK stylesheets without the provided React components

aek-cli - AEK command line interface

## aek-lib

This module contains library code modules for use in developing client side code for AEK.

aek-lib provides useful general utilities and integration points for the AEK and the CampusM platform
with optional React based UI components.

[Docs](https://npm.campusm.net/-/docs/@ombiel/aek-lib/)

## aek-css

This module contains a component driven CSS framework used in the AEK.

aek-css provides a universal, mobile friendly, customizable CSS framework tailored to the AEK

- Built from SemanticUI
- Designed to accompany AEK React Components

[Docs](https://npm.campusm.net/-/docs/@ombiel/aek-css/)

## aek-cli

This module contains the Command Line Interface, which provides tools to create, deploy and manage AEK projects.

aek-cli povides both a human friendly UI that is user driven as well as a
scriptable interface you can utilise within your own automated processes and
be built into shell scripts.

[Docs](https://npm.campusm.net/-/docs/@ombiel/aek-cli/)
