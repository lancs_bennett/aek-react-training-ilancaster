# Deploying an AEK Project to Beta

As an iLancaster developer you will be able to deploy your AEK projects to the iLancaster Beta environment.

## Creating a Menu Options

All AEK projects are accessed in iLancaster through 'Menu Options'.

These Menu Options are tile-based components that can link to one or more AEK projects.
A Menu Option can be styled in many ways but are typically composed of an image.


![alt tag](../assets/sports-16.png)


You cannot create Menu Options for yourself in our Beta environment.

Please contact an iLancaster administrator to setup a Menu Option for your AEK project.

## From Local to Beta

Once you are satisfied with your AEK project locally.
You can deploy this project to the iLancaster Beta environment.

**Note**: You will not be able to deploy an AEK project to the iLancaster Production environment.
This must be done by an iLancaster administrator.

To deploy to the iLancaster Beta environment use the following command:

```bash
aek deploy
```

**Note**: The deploy command does not rebuild the AEK project and deploy a new version. In order to build and deploy a new version, you must now explicitly include the -n flag.

As long as your AEK project has an associated Menu Option
your AEK project will now be available in the Beta environment.

## Updating Beta

To publish your additional AEK project changes to the iLancaster Beta environment
you will need to deploy including the -n flag.

```bash
aek deploy -n
```
