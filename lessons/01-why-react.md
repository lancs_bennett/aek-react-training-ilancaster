# React

React is an emerging JavaScript library for creating user interfaces.
Many people choose to think of React as the V in MVC.

React was built to help build large applications
with data that changes over time.

You can think of React as just a view layer.
Not a Full MV* Framework (compared to Angular, etc).
Everything in React is a component.
Similar to web components, Ember components, or Angular directives.
They simply represent a section of your UI.

## Simple

Simply express how your app should look at any given point in time,
and React will automatically manage all UI updates when your underlying data changes.
This promotes Unidirectional data flow.

## Declarative

When the data changes, React conceptually hits the "refresh" button,
and knows to only update the changed parts.

## Build Composable Components

React is all about building reusable components. In fact,
with React the only thing you do is build components.
This Component Driven Architecture promotes encapsulation,
reuse, testing, and separation of concerns.
