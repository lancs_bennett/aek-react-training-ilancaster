# Weather Example

This weather repository includes some basic example code.

## Weather

Navigate to the aek-examples folder and fire up the local server (make sure you have quit any other servers).

```bash
cd aek-examples
aek start
```

**Note**: Don't forget to select the Beta environment hostname.

### Weather Basic

The Weather Basic AEK project is a simple AEK project contained within one
JSX file.

**Note**: Remember JSX is a declarative syntax used by React.

[Basic-Weather](../code/aek-examples/weather/src/js/weather.jsx)
