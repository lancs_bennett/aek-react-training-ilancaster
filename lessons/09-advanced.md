# Advanced

## How do I get location information?

Please see [Location Picker](https://npm.campusm.net/-/docs/@ombiel/aek-lib/0.2.15/pages/client-tools/location-picker)

## How do I get an image/photo?

Please see [Location Picker](https://npm.campusm.net/-/docs/@ombiel/aek-lib/0.2.15/pages/client-tools/image-picker)

## How do I create multiple pages with routing?

Please see [AEK Router](https://npm.campusm.net/-/docs/@ombiel/aek-lib/0.2.15/pages/other-tools/aek-router)

AEK Router is built on [React Router](https://github.com/reactjs/react-router), the standard routing solution for ReactJS applications.

At its core React Router is just a bunch of React components. We create a router with the `RouterView` component. Let's take a look at a basic app:

```js
var React = require('react');
var {AekReactRouter,RouterView} = require("-components/router");

var router = new AekReactRouter();

var IndexPage = require("./pages/index");

var App = React.createClass({
  render: function() {
    return (
      <RouterView router={router}>
        <IndexPage path="/" />
      </RouterView>
    );
  }
});

React.render(<App/>,document.body);
```

## How do I store data locally?

Please see [AEK Storage](https://npm.campusm.net/-/docs/@ombiel/aek-lib/0.2.15/pages/other-tools/storage)

## How do I launch other parts of the app?

Please see [Screen Link](https://npm.campusm.net/-/docs/@ombiel/aek-lib/0.2.15/pages/other-tools/screen-link)
