# Starting AEK

[Quick Start](https://npm.campusm.net/-/docs/@ombiel/aek-guide/1.0.1/pages/10-quick-start)
**Note**: You may need to login with your iLancaster development credentials

[Basic-AEK](../code/basic-aek)

## Installing packages

First, change directory to the basic AEK project [Basic-AEK](../code/basic-aek).

Second, we'll install the local AEK packages and login:

```bash
aek install
```

## Logging in

To use the AEK tools you must login using your iLancaster development credentials,
you will be prompted to login automatically but if not
we can do this manually by using the following command:

```bash
aek login
```

Please login using your iLancaster development credentials:

```bash
? username joebloggs
? password *********
```

Now you should be logged in

## Running AEK

From the AEK project folder we can run the AEK project locally:

```bash
aek start
```

The AEK command line tools will ask which hostname to target.

Please select the Beta environment hostname:

```bash
lancaster-2.ombiel.co.uk
```

The AEK project is now being driven by the iLancaster Beta environment.

**Note**: 'The aek start' command automatically defaults to port 5000.
You can manually change the port the AEK server listens on.

## Accessing through the browser

You can now interact with this project at:

```
http://localhost:5000
```

So far our we have logged in and run the AEK server locally.
We now need to access this AEK project through the browser.

The AEK server is now targeting the Beta development environment,
to continue to the AEK project please select the following profile:

```bash
iLancaster Staff (LDAP)
```
**Note**: Please enter your Lancaster University credentials not your iLancaster development credentials.

Once you have been authenticated you will be forwarded to your AEK project.

## Extra

Right now the AEK server is listening on port 5000. Cancel the AEK server
and start it again listening on a different port.
