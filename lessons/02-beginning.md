# Beginning

Get everyone to clone the repository.

Go through the slides.

Check everyone has their iLancaster credentials.

Check everyone has prerequisites.


## Documentation

[Installation Details](https://npm.campusm.net/-/docs/@ombiel/aek-cli/0.2.7/pages/20-installation/10-installation-details)
**Note**: You may need to login with your iLancaster development credentials

The following instructions are to get your machine ready for using AEK CLI.
This should take no more than 30 minutes for you to get up and running.

Most of the steps are the same for Windows and OSX.

## Your Credentials

iLancaster development credentials are required to use most of the AEK tools.

You will need sufficient permissions to create and publish AEK Projects.

If you have not received your iLancaster development credentials please
contact the iLancaster team.

## Prerequisites

In order to install aek-cli, there are a few prerequisites. You need a recent version of:

- Git

- NodeJS

- npm

These are common development tools that you may already have.

## Installing the AEK command line interface

To install the aek-cli enter the following command:

```bash
npm install -g https://npm.campusm.net/get/aek-cli
```

This will then download and install AEK CLI globally to your machine.
Once complete you should be able to execute the command:

```bash
aek
```

You should see the AEK command menu.

## Extra

You should now be able to run the following commands:

```bash
git --version
node --version
npm --version
aek --version
```
