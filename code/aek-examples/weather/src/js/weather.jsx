var React = require("-aek/react");
var {VBox,Panel} = require("-components/layout");
var Container = require("-components/container");
var {BannerHeader} = require("-components/header");
var {BasicSegment} = require("-components/segment");
var {Listview,Item} = require("-components/listview");

var request = require("-aek/request");

var Screen = React.createClass({

  getInitialState:function() {
    return {};
  },

  componentDidMount:function() {
    this.getData();
  },

  getData:function() {

    request.get("http://query.yahooapis.com/v1/public/yql/exlibris/weather")
    .query({
      format:"json",
      city:"wolverhampton"
    })
    .end((e,res)=> {
      this.setState({
        weather:res.body.query.results.channel
      });
    });

  },

  render:function() {

    var weather = this.state.weather;
    var loading = !weather;
    var forecast;
    if(!loading) {
      forecast = weather.item.forecast;
    }

    return (
      <Container>
        <VBox>
          <BannerHeader theme="alt" key="header" icon="cloud" flex={0}>Weather</BannerHeader>
          <Panel key="mainPanel">
            <BasicSegment loading={loading}>
              <Listview formatted items={forecast} itemFactory={(day)=>{
                console.log("day",day);
                var thumb = `http://l.yimg.com/a/i/us/we/52/${day.code}.gif`;
                //var thumb = "https://l.yimg.com/a/i/us/we/52/" + day.code + ".gif";
                return (
                  <Item thumbnail={thumb}>
                    <p>{day.day} {day.date}</p>
                    <h3>{day.text}</h3>
                    <p className="small">{day.low}&deg;C - {day.high}&deg;C</p>
                  </Item>
                );
              }}/>
            </BasicSegment>
          </Panel>
        </VBox>
      </Container>
    );
  }
});


React.render(<Screen/>,document.body);
