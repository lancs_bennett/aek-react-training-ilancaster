var React = require("-aek/react");
var {VBox,Panel} = require("-components/layout");
var Container = require("-components/container");
var {BannerHeader} = require("-components/header");
var {AekReactRouter,RouterView} = require("-components/router");


var WeatherPage = require("./pages/weather");
var IndexPage = require("./pages/index");

var router = new AekReactRouter();

var Screen = React.createClass({

  selectCity:function(city) {
    router.goto("/weather/" + city);
  },

  render:function() {

    return (
      <Container>
        <VBox>
          <BannerHeader theme="alt" key="header" icon="cloud" flex={0}>Weather</BannerHeader>
          <Panel key="mainPanel">
            <RouterView router={router}>
              <IndexPage path="/" onSelect={this.selectCity}/>
              <WeatherPage path="/weather/:city" />
            </RouterView>
          </Panel>
        </VBox>
      </Container>
    );
  }
});


React.render(<Screen/>,document.body);
