var React = require("-aek/react");
var Page = require("-components/page");
var {BasicSegment} = require("-components/segment");
var {Listview} = require("-components/listview");

var MyWeather = require("../components/my-weather");

var IndexPage = React.createClass({

  onClick:function(item,ev) {
    ev.preventDefault();
    this.props.onSelect(item);
  },

  render:function() {

    var cities = ["Cardiff","Manchester","London","Wolverhampton","Detroit","Paris"];

    return (
      <Page>
        <BasicSegment>
          <MyWeather/>
          <Listview items={cities} onClick={this.onClick} />
        </BasicSegment>
      </Page>
    );
  }
});

module.exports = IndexPage;
