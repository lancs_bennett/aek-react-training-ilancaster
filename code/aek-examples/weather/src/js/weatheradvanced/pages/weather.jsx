var React = require("-aek/react");
var Page = require("-components/page");
var {BasicSegment} = require("-components/segment");
var {Header} = require("-components/header");
var {Listview,Item} = require("-components/listview");
var request = require("-aek/request");

var WeatherPage = React.createClass({

  getInitialState:function() {
    return {};
  },

  componentDidMount:function() {
    this.getData();
  },

  getData:function() {

    var ctx = this.props.ctx;

    request.get("http://query.yahooapis.com/v1/public/yql/exlibris/weather")
    .query({
      format:"json",
      city:ctx.params.city
    })
    .end((e,res)=> {
      this.setState({
        weather:res.body.query.results.channel
      });
    });

  },

  render:function() {

    var weather = this.state.weather;
    var loading = !weather;
    var forecast,city,country;
    if(!loading) {
      forecast = weather.item.forecast;
      city = weather.location.city;
      country = weather.location.country;
    }


    return (
      <Page>
        <BasicSegment loading={loading}>
          <Header icon="marker" dividing subtext={country}>{city}</Header>
          <Listview formatted items={forecast} itemFactory={(day)=>{
            var thumb = `http://l.yimg.com/a/i/us/we/52/${day.code}.gif`;
            return (
              <Item thumbnail={thumb}>
                <p>{day.day} {day.date}</p>
                <h3>{day.text}</h3>
                <p className="small">{day.low}&deg;C - {day.high}&deg;C</p>
              </Item>
            );
          }}/>
        </BasicSegment>
      </Page>
    );
  }
});

module.exports = WeatherPage;
