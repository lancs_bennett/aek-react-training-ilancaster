var React = require("-aek/react");
var {Segment} = require("-components/segment");
var {Listview} = require("-components/listview");
var request = require("-aek/request");
var Button = require("-components/button");
var LocationPicker = require("-aek/client-tools/location-picker");
var Storage = require("-aek/storage");

var storage = new Storage("cmu-weather-my-weather");


var MyWeather = React.createClass({

  getInitialState:function() {
    return {
      lat:storage.get("lat"),
      lon:storage.get("lon")
    };
  },

  componentDidMount:function() {
    if(this.state.lat && this.state.lon) {
      this.getData(this.state.lat,this.state.lon);
    }
  },

  findLocation:function(ev) {
    ev.preventDefault();
    var locationPicker = new LocationPicker();
    locationPicker.on("complete",(lat,lon,details)=>{
      storage.set("lat",lat);
      storage.set("lon",lon);
      this.setState({lat,lon,details,weather:null});
      this.getData(lat,lon);
    });
  },

  getData:function(lat,lon) {

    request.get("http://query.yahooapis.com/v1/public/yql/exlibris/weather4")
    .query({
      format:"json",
      coords:`(${lat},${lon})`
    })
    .end((e,res)=> {
      this.setState({
        weather:res.body.query.results.channel
      });
    });

  },

  render:function() {
    var state = this.state;
    var weather = this.state.weather;
    var loading = !weather;
    var forecast,city,country;
    if(!loading) {
      forecast = weather.item.forecast;
      city = weather.location.city;
      country = weather.location.country;
    }

    var content;

    if(forecast) {
      var details = forecast[0];
      details = {
        Date:`${details.day} ${details.date}`,
        Text:details.text,
        Temperature:`${details.low}(°C) / ${details.high}(°C)`
      };

      content = [
        <h3>Weather for {city}, {country}:</h3>,
        <Listview uniformLabels basicLabel items={details}/>
      ];
    }

    return (
      <Segment textAlign="center" loading={state.lat && !weather}>
        {content}
        <Button onClick={this.findLocation} variation="prime" size="large" iconBox icon="location arrow">
          Find Location
        </Button>
      </Segment>
    );
  }
});

module.exports = MyWeather;
