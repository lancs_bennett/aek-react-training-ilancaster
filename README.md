# iLancaster AEK Training

This is material I used for Lancaster Universities react training sessions.

The md files in the lessons folder should help you get setup with AEK development.

## Running the examples locally

```bash
cd code/<package>
aek install
aek start
```

Then create new applets by:

```bash
aek create -b ilancaster-boilerplate-aek-react
```

### Credits

A lot of this content was inspired by CampusM available examples / repositories online.

- [The AEK Docs](https://npm.campusm.net/) by CampusM has some great examples and documentation.
